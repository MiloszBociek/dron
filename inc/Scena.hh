#ifndef SCENA_HH
#define SCENA_HH

#include "Dron.hh"
#include "Powierzchnia.hh"
#include "Wzgorze.hh"
#include "Blok.hh"
#include "Plaskowyz.hh"
#include "GraniastoslupPrawidlowy.hh"
#include "Inter_rysowania.hh"

/**
 * @brief Klasa Sceny
 * 
 */
class Scena{
    /**
     * @brief Wektor obiektow w scenie
     * 
     */
    static std::vector<std::shared_ptr<Inter_krajobrazu>> lista_obiektow;
    public:
        /**
         * @brief Konstruktor sceny
         * 
         */
        Scena();
        /**
         * @brief Metoda animujaca obeitky w scenie
         * 
         * @param wysokosc - wysokosc na ktora ma sie wzbic dron
         * @param odleglosc - odleglosc na ktora ma przemiescic sie dron
         * @param kat_stopnie - kat o ktory ma sie obrocic dron
         * @param id - id obiektu
         */
        void animuj(double wysokosc, double odleglosc, double kat_stopnie, unsigned int id);
        /**
         * @brief Dodawanie obiektu do sceny
         * 
         */
        void dodaj();
        /**
         * @brief wypisuje liste wszystkich obiektow w scenie
         * 
         */
        void wypisz_wszystko() const;
        /**
         * @brief Wypisuje liste dronow
         * 
         */
        void wypisz_drony() const;
        /**
         * @brief Wypisuje liste przeszkod
         * 
         */
        void wypisz_przeszkody() const;
        /**
         * @brief Usuwanie obiektu
         * 
         */
        void usun();
        /**
         * @brief Zanjduje w liscie obiektow drona o pdanym id
         * 
         * @param id - szukane id drona
         * @return int - indeks drona w liscie
         */
        int znajdzIdDrona(unsigned int id);
        /**
         * @brief Zanjduje w liscie obiektow przedkody o pdanym id
         * 
         * @param id - szukane id przeszkody
         * @return int - indeks przeszkody w liscie
         */
        int znajdzIdPrzeszkody(unsigned int id);
        /**
         * @brief Usuwanie drona
         * 
         * @param id - ID drona do usuniecia
         */
        void usunDrona(unsigned int id);
        /**
         * @brief Usuwa przeszkode
         * 
         * @param id - ID przeszkody do usuniecia
         */
        void usunPrzeszkode(unsigned int id);
        /**
         * @brief Metoda pobierania listy obiektow w scenie
         * 
         * @return std::vector<std::shared_ptr<Inter_krajobrazu>>& - lista obiektow w scenie
         */
        static std::vector<std::shared_ptr<Inter_krajobrazu>> &pobierzListe() {return lista_obiektow;};

};

#endif