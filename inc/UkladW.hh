#ifndef UKLADW_HH
#define UKLADW_HH

#include "Wektor.hh"
#include "MacierzRot.hh"
#include "Dr3D_gnuplot_api.hh"
/**
 * @brief Klasa ukaldu wspolrzednych
 * 
 */
class UkladW{
    protected:
        /**
         * @brief Zmienna przechuwujaca wskaznik do zewnetrznego ukladu wspolrzednych
         * 
         */
        UkladW * poprzednik;
        /**
         * @brief wektor polozenia ukladu wspolrzednych
         * 
         */
        Wektor<3> _srodek;
        /**
         * @brief Macierz orientacji ukaldu wspolrzednych
         * 
         */
        MacierzRot<3> _orientacja;
    public:
    /**
     * @brief Konstruktor ukladu wspolrzednych
     * 
     */
        UkladW() = default;
        /**
         * @brief Parametryczny konstruktor ukladu wspolrzednych
         * 
         * @param sr - wektor polozenia ukladu
         * @param orientacja  - macierz orientacji ukladu
         * @param rodzic - wskaznik na zewnetrzny uklad wspolrzednych
         */
        UkladW(const Wektor<3> &sr,const MacierzRot<3> &orientacja, UkladW * rodzic) : poprzednik(rodzic), _srodek(sr), _orientacja(orientacja) {};
        /**
         * @brief Metoda Translacji - przesuniecia
         * 
         * @param W - Wektor o ktory przesuwa sie uklad
         */
        void Translacja(const Wektor<3> & W){_srodek = _srodek + W;};
        /**
         * @brief Metora rotacji ukladu
         * 
         * @param M - macierz rotacji o ktora obraca sie uklad
         */
        void Rotacja(const MacierzRot<3> & M){_orientacja = _orientacja * M;};
        /**
         * @brief Meotda konwersji Wektora<3> do punktu
         * 
         * @param wektor - wektor 3 wymiarowy 
         * @return drawNS::Point3D - punkt 
         */
        drawNS::Point3D konwersja(Wektor<3> wektor);
        /**
         * @brief Metoda przeliczania wspolrzednych pounktu w ukladzie do ukladu globalnego
         * 
         * @param punkt_lokalny - wektor polozenia punktu we wspolrzednych lokalnych
         * @return Wektor<3> - wektor polozenia punktu we wspolrzednych globalnych
         */
        Wektor<3> przelicz_do_globalnego(Wektor<3> punkt_lokalny);
        /**
         * @brief Metoda przeliczania punktu do zewnetrznego ukladu wspolrzednych
         * 
         * @param punkt_lokalny - wektor polozenia punktu w domyslnym ukladzie wspolrzednych
         * @return Wektor<3> - wektor polozenia punktu w zewnewtrznym ukladzie wspolrzednych
         */
        Wektor<3> przelicz_punkt_do_rodzica( Wektor<3> punkt_lokalny);
};
#endif