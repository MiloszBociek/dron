#ifndef BLOK_HH
#define BLOK_HH

#include "UkladW.hh"
#include "Inter_krajobrazu.hh"
#include "Inter_rysowania.hh"
#include "Przeszkoda.hh"

/**
 * @brief Klasa Blok - prostopadloscian o podstawie prostokata
 * 
 */
class Blok : public Przeszkoda, public Inter_krajobrazu, public Inter_rysowania{

  /**
   * @brief Zmienna przechowujaca id bloku
   * 
   */
  int id;

  public:
    /**
     * @brief Konstruktow bloku
     * 
     * @param sr - srodek ukladu wspolrzednych bloku
     * @param orientacja - macierz rotacji dla bloku
     * @param wysokosc - wysokoc bloku
     * @param szerokosc - szerokosc bloku
     * @param glebokosc - glebokosc bloku
     */
    Blok(const Wektor<3> &sr,const MacierzRot<3> &orientacja, const double &wysokosc, const double &szerokosc, const double &glebokosc ) : Przeszkoda(sr, orientacja, wysokosc, szerokosc, glebokosc), id(-1){
      this->rysuj();
    };
    /**
     * @brief Metoda rysowania bloku
     * 
     */
    void rysuj() override;
    /**
     * @brief Metoda przeliczania wierzcholkow dla bloku
     * 
     * @return std::array<Wektor<3>, 8> - wektor punktow (Wektorow) z ktorych sklada sie blok
     */
    std::array<Wektor<3>, 8> Przelicz();
    /**
     * @brief Metoda zwracajaca typ obiektu w postaci ciagu znakow
     * 
     * @return std::string - ciag znakow reprezentujacy typ obiektu
     */
    std::string typ() override { return "Blok"; };
    /**
     * @brief Metoda zwracajaca wektor polozenia dla bloku
     * 
     * @return Wektor<3> - Wektor polozenia bloku
     */
    Wektor<3> polozenie() override { return this->_srodek; };
    /**
     * @brief Metoda zwracajaca wysokosc dla bloku
     * 
     * @return double - Wysokosc bloku
     */
    double pobierzWysokosc() override {return this->wysokosc;};
    /**
     * @brief Metoda sprawdzajaca czy nad blokiem jest inny obiekt
     * 
     * @param innyDron - wskaznik na inny obiekt
     * @param wysokosc - wzpolzedna z obiektu
     * @return true - kiedy obiekt jest nad blokiem (dodany offset do pola bloku)
     * @return false - kiedy obiekt nie jest nad blokiem (dodany offset do pola bloku)
     */
    bool czy_nad(Inter_drona * innyDron, double wysokosc) override;
    /**
     * @brief Metoda sprawdzajaca czy mozna wylodowac na bloku
     * 
     * @param innyDron - wskaznik na inny obiekt
     * @param wysokosc - wzpolzedna z obiektu
     * @return true - kiedy srodek obiektu jest nad blokiem
     * @return false - kiedy srodek obiektu nie jest nad blokiem
     */
    bool czy_ladowac(Inter_drona * innyDron, double wysokosc) override;
    /**
     * @brief Metoda zwracajaca id bloku
     * 
     * @return int - id bloku
     */
    int getId(){return id;};
    /**
     * @brief Destruktor bloku
     * 
     */
    ~Blok()
    {
        Inter_rysowania::get()->erase_shape(this->id);
        Inter_rysowania::get()->redraw();
    };
};



#endif