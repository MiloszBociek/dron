#ifndef INTER_DRONA_HH
#define INTER_DRONA_HH
/**
 * @brief Klasa interfejsu drona
 * 
 */
class Inter_drona{
    public:
        /**
         * @brief Wirtualna metoda - obraca wirniki drona z zadana predkoscia
         * 
         * @param predkosc - predkosc obrotu wirnikow
         */
        virtual void obroc_wirniki(const double & predkosc) = 0;
        /**
         * @brief Wirtualna metoda - porusza dronem w kirunku zwrotu drona
         * 
         * @param ile - Okresla na ile ma nastapic przesuniecie
         */
        virtual void lec_w_przod(const double & ile) = 0;
        /**
         * @brief Wirtualna metoda - porusza dronem w kierunku gora-dol
         * 
         * @param ile - Okresla na ile ma nastapic przesuniecie
         */
        virtual void lec_w_gore(const double & ile) = 0;
        /**
         * @brief Wirtualna metoda - obraca drona o zadany kat wokol osi Z
         * 
         * @param kat - Kat o ktory dron ma sie obrocic (w stopniach)
         */
        virtual void obroc(const double & kat) = 0;
        /**
         * @brief Wirtualna metoda animujaca ruch drona
         * 
         * @param wysokosc - Wysokosc przelotowa
         * @param dlugosc - Odleglosc przelotu
         * @param kat - Kat obrotu (kierunek lotu)
         */
        virtual void animacja(const double &wysokosc, const double &dlugosc, const double &kat) = 0;
        /**
         * @brief Getter do pobierania promienia drona
         * 
         * @return promien drona
         */
        virtual double pobierzR() = 0;
        /**
         * @brief Wirtualny destruktor drona
         * 
         */
        virtual ~Inter_drona() {}
};

#endif