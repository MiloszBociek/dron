#ifndef PROSTOPADLOSCIAN_HH
#define PROSTOPADLOSCIAN_HH

#include <unistd.h>
#include "UkladW.hh"
#include "Wektor.hh"
#include "MacierzRot.hh"
#include "Inter_rysowania.hh"
#include "Dr3D_gnuplot_api.hh"
/**
 * @brief Klasa Prostopadloscianu
 * 
 */
class Prostopadloscian : public UkladW, Inter_rysowania {
    /**
     * @brief Wysokosc prostopadloscaianu
     * 
     */
    double _wysokosc;
    /**
     * @brief Dlugosc prostopadloscainu
     * 
     */
    double _dlugosc;
    /**
     * @brief Szerokosc prostopadloscianu
     * 
     */
    double _szerokosc;
    /**
     * @brief Id prostopadloscianu
     * 
     */
    int id;
public:
/**
 * @brief Konstruktor prostopadloscianu
 * 
 * @param polozenie - wektor polozenia
 * @param orientacja - macierz orientacji
 * @param rodzic - zewnetrzny uklad wspolrzednych
 * @param wysokosc - wysokosc figury
 * @param dlugosc - dlugosc figury
 * @param szerokosc - szerokosc figury
 */
    Prostopadloscian(const Wektor<3> &polozenie,const MacierzRot<3> &orientacja, UkladW * rodzic, const double &wysokosc, const double &dlugosc, const double &szerokosc) : 
    UkladW(polozenie, orientacja, rodzic), _wysokosc(wysokosc), _dlugosc(dlugosc), _szerokosc(szerokosc), id(-1) {};
    /**
     * @brief Metoda rysowania prostopadloscianu
     * 
     */
    void rysuj() override;
    /**
     * @brief Metoda zwracjaca  ID prostopadloscianu
     * 
     * @return int - ID prostopadloscianu
     */
    int getId() {return id;};
    /**
     * @brief Meotda przeliczajaca wspolrzedne wierzcholkow figury
     * 
     * @return std::array<Wektor<3>, 8> - tablica wierzcholkow
     */
    std::array<Wektor<3>, 8> Przelicz();
};
#endif