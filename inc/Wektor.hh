#ifndef WEKTOR_HH
#define WEKTOR_HH

#include <limits>
#include <cmath>
#include <array>
#include <iostream>

/**
 * @brief Klasa Wektora
 * 
 * @tparam ROZMIAR - Rozmiar wektora (liczba wymiarow) 
 */
template <unsigned int ROZMIAR>
class Wektor {
/**
 * @brief Tablica przechowujaca wspolrzedne wektora
 * 
 */
  std::array<double, ROZMIAR> wektor;
  /**
   * @brief Zmienna przechowujaca liczbe obecnych wektorow
   * 
   */
  inline static int _ile_jest = 0;
  /**
   * @brief Zmienna przechowujaca liczbe stworzonych wektorow
   * 
   */
  inline static int _ile_stworzono = 0;

  public:
  /**
   * @brief Konstruktor wektora
   * 
   */
  Wektor() : wektor{} { _ile_jest++; _ile_stworzono++;};
  /**
   * @brief Parametryczny konstruktor wektora
   * 
   * @param arg - Tablica wspolrzednych wektora
   */
  Wektor(std::array<double, ROZMIAR> arg) : wektor(arg) {_ile_jest++; _ile_stworzono++;};
  /**
   * @brief Konstruktor kopiujacay wektora
   * 
   * @param W - Wektor do skopiowania
   */
  Wektor(const Wektor &W) : wektor(W.wektor) {_ile_jest++; _ile_stworzono++;};

  /**
   * @brief Operator przypisania wektora
   * 
   * @param arg2 - wktor ktory chcemy przypisac
   * @return Wektor<ROZMIAR> - wektor do ktorego przypisujemy
   */
  Wektor<ROZMIAR> operator = (const Wektor<ROZMIAR> & arg2);
/**
 * @brief Operator dodawania wektorow
 * 
 * @param arg2 - wektor ktory dodajemy
 * @return Wektor<ROZMIAR> - suwa wektorow
 */
  Wektor<ROZMIAR> operator + (const Wektor<ROZMIAR> & arg2) const;
/**
 * @brief Operator odejmowania wektorow
 * 
 * @param arg2 - wektor ktory odejmujemy
 * @return Wektor<ROZMIAR> - roznica wektorow
 */
  Wektor<ROZMIAR> operator - (const Wektor<ROZMIAR> & arg2) const;
  /**
   * @brief Operator mnozenia wektora przez liczbe
   * 
   * @param arg2 - liczba przez kotra mnozymy wektor
   * @return Wektor<ROZMIAR> - pomnozony wektor
   */
  Wektor<ROZMIAR> operator * (const double & arg2) const;
  /**
   * @brief Operator mnozenia wektora przez wektor
   * 
   * @param arg2 - wektor przez ktory mnozymy
   * @return double - pomnozony wektor
   */
  double operator * (const Wektor<ROZMIAR> & arg2) const;
  /**
   * @brief Metoda zwracajaca dlugosc wektora
   * 
   * @return double - dlugosc wektora
   */
  double dlugosc() const;
  /**
   * @brief Meodta obliczajaca liczbe aktywnych wektorow
   * 
   * @return int - liczba obecnych wktorow
   */
  static int ile_jest(){return _ile_jest;};
  /**
   * @brief Mtoda zwracajaca liczbe stworzonych wektorow
   * 
   * @return int - ile stworzonych wektorow ogolnie
   */
  static int ile_stworzono(){return _ile_stworzono;};
  /**
   * @brief Operator pobierania wspolrzednej wektora
   * 
   * @param indeks - Indeks wspolrzednej wektora
   * @return const double& - Wspolrzedna wektora
   */
  const double & operator [] (unsigned int indeks) const;
  /**
   * @brief Metoda ustawiajaca wspolrzedne wektora
   * 
   * @param indeks - Indeks wspolrzednej wektora
   * @return const double& - Wspolrzedna wektora
   */
  double & operator [] (unsigned int indeks); //set
  /**
   * @brief Destruktor wektora
   * 
   */
  ~Wektor(){_ile_jest--;};


};
/**
 * @brief Funkcja wcztyujaca wektor ze strumienia
 * 
 * @tparam ROZMIAR - rozmair wektora
 * @param Strm - strumien
 * @param wektor - wczytywany wektor
 * @return std::istream& - strumien z ktorego wczytujemy
 */
template <unsigned int ROZMIAR>
std::istream& operator >> (std::istream &Strm, Wektor<ROZMIAR> &wektor);

/**
 * @brief Funkcja wypisywania wektora na strumien
 * 
 * @tparam ROZMIAR - rozmiar wektora
 * @param Strm - strumien na ktory sie wypisuje
 * @param wektor - wypisywany wektor
 * @return std::ostream& - strumien na ktory wypisywany jest wketor
 */
 template <unsigned int ROZMIAR>
std::ostream& operator << (std::ostream &Strm, const Wektor<ROZMIAR> &wektor);


#endif
