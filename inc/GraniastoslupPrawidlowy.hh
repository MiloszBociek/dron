#ifndef GRANIASTOSLUPPRAWIDLOWY_HH
#define GRANIASTOSLUPPRAWIDLOWY_HH
#include <random>
#include <ctime>
#include <chrono>
#include "UkladW.hh"
#include "Scena.hh"
#include "Inter_krajobrazu.hh"
#include "Inter_rysowania.hh"
#include "Przeszkoda.hh"
/**
 * @brief Klasa Graniastoslup Prawidlowy
 * 
 */
class GraniastoslupPrawidlowy : public Przeszkoda, public Inter_krajobrazu, public Inter_rysowania
{
    /**
     * @brief Zmienna przechowujaca pole graniastoslupa
     * 
     */
    int id;
    /**
     * @brief Zmienna przechowujaca minimalny promien graniastoslupa
     * 
     */
    double R_min;
        /**
     * @brief Zmienna przechowujaca maksymalny promien graniastoslupa
     * 
     */
    double R_max;

public:
    /**
     * @brief Konstruktor graniastoslupa
     * 
     * @param sr - wektor polozenia figury
     * @param orientacja - orientacja figury
     * @param wysokosc - wysokosc figury
     * @param Min_Max - para przechowujaca wartosc minimalna i maksymalna promienia figury
     */
    GraniastoslupPrawidlowy(const Wektor<3> &sr, const MacierzRot<3> &orientacja, const double &wysokosc, std::pair<const double, const double> Min_Max) : Przeszkoda(sr, orientacja, wysokosc, Min_Max), id(-1){
        this->rysuj();
    };
    /**
     * @brief metoda rysowania graniastoslupa
     * 
     */
    void rysuj() override;
    /**
     * @brief Metoda przeliczajaca wspolrzedne dla wierzcholkow graniastoslupa 
     * 
     * @return std::vector<Wektor<3>>  - wektor przechowujacy wierzcholki graniastoslupa
     */
    std::vector<Wektor<3>> Przelicz();
    /**
     * @brief Metoda zwracajaca typ obiektu
     * 
     * @return std::string - ciag znakow reprezentujacy typ obiektu ("Plaskowyz prawidlowy")
     */
    std::string typ() override { return "Plaskowyz prawidlowy"; };
    /**
     * @brief Metoda zwracjaca wektor polozenia obiektu
     * 
     * @return Wektor<3> - wektor polozenia obiektu
     */
    Wektor<3> polozenie() override { return this->_srodek; };
    /**
     * @brief Metoda zwracajaca wysokosc obiektu
     * 
     * @return double - wysokosc obiektu
     */
    double pobierzWysokosc() override {return this->wysokosc;};
    /**
     * @brief Metoda sprawdzajaca czy inny obiekt jest nad graniastoslupem
     * 
     * @param dron - wskaznik na inny obiekt 
     * @param wysokosc - wysokosc na jakiej znajduje sie inny obiekt
     * @return true - gdy obiekt jest nad graniastoslupem
     * @return false - gry obiekt nie jest na graniastoslupie
     */
    bool czy_nad(Inter_drona * dron, double wysokosc) override;
    /**
     * @brief Metoda sprawdzajaca czy mozna lodowac na graniastoslupie
     * 
     * @param dron - wskaznik na inny obiekt (drona)
     * @param wysokosc - wysokosc na jakiej znajduje sie dron
     * @return true - gdy mozna lodowac
     * @return false - gdy nie mozna lodowac
     */
    bool czy_ladowac(Inter_drona * dron, double wysokosc) override;
    /**
     * @brief Destruktor graniastoslupa
     * 
     */
    ~GraniastoslupPrawidlowy()
    {
        Inter_rysowania::get()->erase_shape(this->id);
        Inter_rysowania::get()->redraw();
    };
};

#endif