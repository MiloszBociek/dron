#ifndef INTER_PRZESZKODY_HH
#define INTER_PRZESZKODY_HH

#include <chrono>
#include <random>
#include "UkladW.hh"
/**
 * @brief Klasa Interfejsu przeszkody
 * 
 */

class Przeszkoda
 : protected UkladW {
    protected:
    /**
     * @brief Wysokość przeszkód
     * 
     */
        double wysokosc;
     /**
     * @brief Szerokosc przeszkód
     * 
     */
        double szerokosc;
     /**
     * @brief glebokosc przeszkód
     * 
     */
        double glebokosc;
    /**
     * @brief Para liczb ograniczajacych zakres minimal nego i maksymalnego oddalenia wierzcholkow podstawy od srodka przeszkody
     * 
     */
        std::pair<double, double> min_max;
    /**
     * @brief Ilosc wierzcholkow w podstawie bryly
     * 
     */
        int wierzcholki;

    public:
    /**
     * @brief Konstruktor obiektu klasy Inter_rysowania dla graniastoslupow
     * 
     * @param polozenie - środek bryly
     * @param orientacja - macierz orientacji bryly
     * @param _wysokosc - wysokkosc bryly
     * @param Min_Max  - para liczb (oddalenie wierzcholkow od podstawy)
     */
        Przeszkoda(Wektor<3> polozenie, MacierzRot<3> orientacja ,double _wysokosc, std::pair<double, double> Min_Max) : 
        UkladW(polozenie, orientacja, nullptr), wysokosc(_wysokosc), min_max(Min_Max.first, Min_Max.second) { wierzcholki=this->losuj(3,9);};
         /**
          * @brief Konstruktor obiektu klasy Inter_rysowania dla bloku
          * 
          * @param polozenie 
          * @param orientacja 
          * @param _wysokosc 
          * @param _szerokosc 
          * @param _glebokosc 
          */
        Przeszkoda(Wektor<3> polozenie, MacierzRot<3> orientacja ,double _wysokosc, double _szerokosc, double _glebokosc) : 
        UkladW(polozenie, orientacja, nullptr), wysokosc(_wysokosc), szerokosc(_szerokosc), glebokosc(_glebokosc), wierzcholki(8) {};

         /**
          * @brief Construct a new Przeszkoda object
          * 
          * @param polozenie 
          * @param orientacja 
          */
        Przeszkoda(Wektor<3> polozenie, MacierzRot<3> orientacja, UkladW *rodzic) : 
        UkladW(polozenie, orientacja, rodzic), wysokosc(0), szerokosc(0), glebokosc(0), wierzcholki(0) {};
        /**
         * @brief Metoda losująca liczbe z podanego predzialu Min Max
         * 
         * @param Min - Minimalna mozliwa wylosowana liczba
         * @param Max  - Maksymalna mozliwa wylosowana liczba
         * @return double - zwraca liczbe z podanego zakresu
         */
        double losuj(double Min, double Max);
         /**
          * @brief Wirtualna metoda zwracajaca wysokosc obiektu 
          * 
          * @return double - Wysokosc obiektu
          */
        virtual double pobierzWysokosc() = 0;
        /**
         * @brief Wirtualny destruktor obiektu klasy Inter_przeszkody
         * 
         */
        virtual ~Przeszkoda(){};
};

#endif