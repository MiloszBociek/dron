#ifndef MACIERZROT_HH
#define MACIERZROT_HH

#include <iostream>
#include "Wektor.hh"
#include <cmath>

/**
 * @brief 
 * 
 */
enum OS {OX, OY, OZ};

/**
 * @brief Klasa Macierzy (kwadratowej)
 * 
 * @tparam ROZMIAR - Rozmiar macierzy
 */
template <unsigned int ROZMIAR>
class MacierzRot {
  /**
   * @brief Tablica wektorow - macierz
   * 
   */
  std::array<Wektor<ROZMIAR>, ROZMIAR> macierz;

  public:
  /**
   * @brief Konstruktor macierzy rotacji - tworzy macierz jednostkowa
   * 
   */
  MacierzRot();
  /**
   * @brief Konstruktor macierzy rotacji
   * 
   * @param kat_stopnie - kat o ktory nalezy obrocic obiekt
   * @param o - os wzgledem ktorej nastopi obrot
   */
  MacierzRot(const double & kat_stopnie, OS o);
  /**
   * @brief Metoda transpozycji macierzy
   * 
   * @return MacierzRot<ROZMIAR> - Macierz transponowana
   */
  MacierzRot<ROZMIAR> transpozycja() const;
  /**
   * @brief Operator mnozenia macierzy przez wektor
   * 
   * @param arg2 - wektor przez ktory mnozymy macierz
   * @return Wektor<ROZMIAR> - macierz pomnozona przez wektor
   */
  Wektor<ROZMIAR> operator * (const Wektor<ROZMIAR> & arg2) const;
  /**
   * @brief Operator mnozenia macierzy przez macierz
   * 
   * @param arg2 - Macierz przez ktora mnozymy
   * @return MacierzRot<ROZMIAR> - macierz pomnozona przez macierz
   */
  MacierzRot<ROZMIAR> operator * (const MacierzRot<ROZMIAR> & arg2) const;
  /**
   * @brief Operator pobierajacy rzad z macierzy
   * 
   * @param indeks - indeks rzedu
   * @return const Wektor<ROZMIAR>& - wektor (rzad macierzy) 
   */
  const Wektor<ROZMIAR> & operator [] (unsigned int indeks) const;
  /**
   * @brief Operator ustawiajacy rzad w macierzy 
   * 
   * @param indeks - indeks rzedu ktory mamy ustawic
   * @return Wektor<ROZMIAR>& - wektor (ustawiany rzad macierzy)
   */
  Wektor<ROZMIAR> & operator [] (unsigned int indeks);
  /**
   * @brief Operator przypisywania macierzy do innej macierzy
   * 
   * @param mac - macierzy do ktorej przypisujemy 
   * @return MacierzRot<ROZMIAR>& - przypisywana macierz
   */
  MacierzRot<ROZMIAR> & operator = (const MacierzRot<ROZMIAR> &mac);

};

/**
 * @brief Metoda wypisujaca macierz
 * 
 * @tparam ROZMIAR - Rozmiar macierzy
 * @param Strm - Strumien na ktory wypisywana jest macierz
 * @param macierz - macierz do wypisania
 * @return std::ostream& - strumien na ktory wuypisywana jest macierz
 */
template <unsigned int ROZMIAR>
std::ostream & operator << (std::ostream &Strm, const MacierzRot<ROZMIAR> &macierz);
/**
 * @brief 
 * 
 * @param strm - Strumien z ktorego wczytywana jest macierz
 * @param os - os wokol ktorej obracamy obiekt
 * @return std::istream& - strumien z ktorego wczytywana jest macierz
 */
std::istream &operator>>(std::istream &strm, OS &os);


#endif
