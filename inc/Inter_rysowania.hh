#ifndef INTER_RYSOWANIA_HH
#define INTER_RYSOWANIA_HH

#include "Dr3D_gnuplot_api.hh"
/**
 * @brief klasa Interfejsu Rysowania
 * 
 */
class Inter_rysowania{
    protected:
        /**
         * @brief Id obiektu
         * 
         */
        int id;
        /**
         * @brief Wskasnik na rysownik
         * 
         */
        static std::shared_ptr<drawNS::Draw3DAPI> rysownik;
    public:
        /**
         * @brief Metoda zwracajaca wskaznik wskaznik na rysownik
         * 
         * @return std::shared_ptr<drawNS::Draw3DAPI> - wskaznik na rysownik
         */
        std::shared_ptr<drawNS::Draw3DAPI> get(){return rysownik;}
        /**
         * @brief Wirtualna metoda rysowania
         * 
         */
        virtual void rysuj()=0;
        /**
         * @brief Destruktor obiektu klasy Interfejs rysowania
         * 
         */
        virtual ~Inter_rysowania() {}
};

#endif