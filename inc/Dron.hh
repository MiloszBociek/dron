#ifndef DRON_HH
#define DRON_HH

#include "Graniastoslup6.hh"
#include "Prostopadloscian.hh"
#include "Inter_krajobrazu.hh"
#include "Przeszkoda.hh"
#include "Scena.hh"
#include <thread>
#include <chrono>

#define wysokosc_calkowita 2
#define promien_w 0.75
#define dglebokosc 4
#define dwysokosc 1
#define dszerokosc 2

/**
 * @brief Klasa drona
 * 
 */
class Dron : public Przeszkoda, public Inter_rysowania, public Inter_drona, public Inter_krajobrazu
{
    /**
     * @brief Korpus drona
     * 
     */
    Prostopadloscian korpus;
    /**
     * @brief Tablica przechowujaca 4 wirniki
     * 
     */
    std::array<Graniastoslup6, 4> wirniki;
    /**
     * @brief Tablica przechowujaca 4 tablice ktore przechowuja po 2 prostopadlosciany (4x2 lopatki wirnika)
     * 
     */
    std::array<std::array<Prostopadloscian, 2>, 4> smigla;
    /**
     * @brief Zmienna przechowujaca promien drona
     * 
     */
    double poleR;

public:
/**
 * @brief Konstruktor drona
 * 
 * @param sr - wektor polozenia drona
 * @param orientacja - macierz rotacji drona
 * @param rodzic - w tym przypadku uklad wspolrzdednych (rodzic dla drona)
 */
    Dron(const Wektor<3> &sr, const MacierzRot<3> &orientacja, UkladW *rodzic = nullptr) : 
    Przeszkoda(sr, orientacja, rodzic),
    korpus(Wektor<3>({0, 0, 0.5}), MacierzRot<3>(), this, dwysokosc, dszerokosc, dglebokosc),
    wirniki({
    Graniastoslup6(Wektor<3>({1, 1, 1}), MacierzRot<3>(), this, promien_w, 0.2), 
    Graniastoslup6(Wektor<3>({-1, 1, 1}), MacierzRot<3>(), this, promien_w, 0.2),
    Graniastoslup6(Wektor<3>({1, -1, 1}), MacierzRot<3>(), this, promien_w, 0.2), 
    Graniastoslup6(Wektor<3>({-1, -1, 1}), MacierzRot<3>(), this, promien_w, 0.2)
    }),
    smigla({{
    {Prostopadloscian(Wektor<3>({1, 1, 1}), MacierzRot<3>(), this, 0.025, 1, 0.1), Prostopadloscian(Wektor<3>({1, 1, 1}), MacierzRot<3>(45, OS::OZ), this, 0.025, 1, 0.1)},
    {Prostopadloscian(Wektor<3>({-1, 1, 1}), MacierzRot<3>(), this, 0.025, 1, 0.1), Prostopadloscian(Wektor<3>({-1, 1, 1}), MacierzRot<3>(45, OS::OZ), this, 0.025, 1, 0.1)},
    {Prostopadloscian(Wektor<3>({1, -1, 1}), MacierzRot<3>(), this, 0.025, 1, 0.1), Prostopadloscian(Wektor<3>({1, -1, 1}), MacierzRot<3>(45, OS::OZ), this, 0.025, 1, 0.1)},
    {Prostopadloscian(Wektor<3>({-1, -1, 1}), MacierzRot<3>(), this, 0.025, 1, 0.1), Prostopadloscian(Wektor<3>({-1, -1, 1}), MacierzRot<3>(45, OS::OZ), this, 0.025, 1, 0.1)}
    }})
    {
    poleR = this->pobierzR();
    this->rysuj();
    };
    

    /**
     * @brief Metoda obracajaca wirniki (stopnie)
     * 
     * @param predkosc - kat o jaki maja sie obrocic wirniki
     */
    void obroc_wirniki(const double & predkosc) override;
    /**
     * @brief Metoda przesuwajaca drona do produ o zadana odleglosc
     * 
     * @param ile - odleglosc o jaka ma sie przemiescic dron
     */
    void lec_w_przod(const double & ile) override;
    /**
     * @brief Metoda przesuwajaca drona do gory o zadana odleglosc
     * 
     * @param ile odleglosc o jaka ma sie przemiescic dron
     */
    void lec_w_gore(const double & ile) override;
    /**
     * @brief Metoda obracajaca drona o zadany kat (stopnie)
     * 
     * @param kat - kot o jaki ma sie obrocic dron
     */
    void obroc(const double & kat) override;
    /**
     * @brief Metoda rysowania drona
     * 
     */
    void rysuj() override;
    /**
     * @brief Metoda animacji drona
     * 
     * @param wysokosc - wysokos na jaka ma sie wzbic
     * @param dlugosc - odleglosc ktora ma przeleciec
     * @param kat - kat o ktory ma sie obrocic
     */
    void animacja(const double &wysokosc, const double &dlugosc, const double &kat) override;
    /**
     * @brief Metoda zwracajaca typ obiektu (Dron)
     * 
     * @return std::string - ciag znakow reprezentujacy typ drona
     */
    std::string typ() override {return "Dron";};
    /**
     * @brief Wektor polozenia drona
     * 
     * @return Wektor<3> - Wektor polozenia drona
     */
    Wektor<3> polozenie() override {return this->_srodek;};
    /**
     * @brief Metoda zwracajaca promien drona
     * 
     * @return double - promien drona
     */
    double pobierzR() override;
    /**
     * @brief Metoda zwracajaca wysokosc przeszkody (drona w tym przypadku)
     * 
     * @return double - Wysokosc drona
     */
    double pobierzWysokosc() override {return wysokosc_calkowita;};
    /**
     * @brief Metoda sprawdzajaca czy nad dronem znajduje sie inny obiekt 
     * 
     * @param innyDron - wskaznik na inny obiekt (inny dron w tym przypadku)
     * @param wysokosc - Wysokosc na ktorej znajduje sie inny dron
     * @return true - jesli nad dronem jest inny obiekt
     * @return false - jezeli nad dronem nie ma innego obiektu
     */
    bool czy_nad(Inter_drona * innyDron, double wysokosc) override;
    /**
     * @brief Metoda sprawdzajaca czy mozna wylodowac na dronie
     * 
     * @param innyDron - wskaznik na inny obiekt
     * @param wysokosc - wysokosc na ktorej jest inny obiekt
     * @return true - nigdy
     * @return false - zawsze
     */
    bool czy_ladowac(Inter_drona * innyDron, double wysokosc) override;
    /**
     * @brief Destruktor drona
     * 
     */
    ~Dron();
};

#endif