#ifndef PLASKOWYZ_HH
#define PLASKOWYZ_HH
#include <random>
#include <ctime>
#include <chrono>
#include "UkladW.hh"
#include "Inter_krajobrazu.hh"
#include "Inter_rysowania.hh"
#include "Przeszkoda.hh"
/**
 * @brief Klasa Plaskowyz
 * 
 */
class Plaskowyz : public Przeszkoda, public Inter_krajobrazu, public Inter_rysowania
{
    /**
     * @brief Zmienna przechowujaca id obiektu
     * 
     */
    int id;
    /**
     * @brief Zmienna przechowywujaca najwiekszy promien podstawy
     * 
     */
    double R_max;
        /**
     * @brief Zmienna przechowywujaca najmniejszy promien podstawy
     * 
     */
    double Rg_min;
public:
/**
 * @brief Konstruktor Plaskowyzu
 * 
 * @param sr - wektor polozenia plaskowyzu
 * @param orientacja - macierz orientacji plaskowyzu
 * @param wysokosc - wysokosc plaskowyzu
 * @param Min_Max - Para przechowujaca minimalna i maksymalna wartosc dla promienia
 */
    Plaskowyz(const Wektor<3> &sr, const MacierzRot<3> &orientacja, const double &wysokosc, std::pair<const double, const double> Min_Max) : Przeszkoda(sr, orientacja, wysokosc, Min_Max), id(-1){
        this->rysuj();
    };
    /**
     * @brief Metoda rysujaca plaskowyz
     * 
     */
    void rysuj() override;
    /**
     * @brief Metoda obliczajace wspolrzedne dla wierzcholkow figury
     * 
     * @return std::vector<Wektor<3>> - wektor przechowujacy wierzcholki plaskowyzu
     */
    std::vector<Wektor<3>> Przelicz();
    /**
     * @brief Metoda zwracajaca typ obiektu
     * 
     * @return std::string - ciag znakow reprezentujacy typ obiektu ("Plaskowyz") 
     */
    std::string typ() override { return "Plaskowyz"; };
    /**
     * @brief Metoda zwracajaca wektor polozenia figury
     * 
     * @return Wektor<3> - wektor polozenia figury
     */
    Wektor<3> polozenie() override { return this->_srodek; };
    /**
     * @brief Metoda zwracajaca wysokosc figury
     * 
     * @return double - wysokosc figury
     */
    double pobierzWysokosc() override {return this->wysokosc;};
    /**
     * @brief Metoda sprawdzajaca czy inny obiekt znajduje sie nad plaskowyzem
     * 
     * @param dron - wskaznik na inny obiekt
     * @param wysokosc - wysokosc na jakiej znajduje sie inny obiekt
     * @return true - gdy obiekt jest nad figura
     * @return false - gdy obiekt nie jest nad figura
     */
    bool czy_nad(Inter_drona * dron, double wysokosc) override;
    /**
     * @brief Metoda sprawdzajaca czy mozna ladowac na plaskowyzu
     * 
     * @param dron - wskaznik na inny obiekt
     * @param wysokosc - wysokosc na jakiej znajduje sie inny obiekt
     * @return true - gdy obiekt moze ladowac
     * @return false - gdy obiekt nie moze lodowac
     */
    bool czy_ladowac(Inter_drona * dron, double wysokosc) override;
    /**
     * @brief Destruktor Plaskowyzu
     * 
     */
    ~Plaskowyz()
    {
        Inter_rysowania::get()->erase_shape(this->id);
        Inter_rysowania::get()->redraw();
    };
};

#endif