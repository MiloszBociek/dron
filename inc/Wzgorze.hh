#ifndef OSTROSLUPN_HH
#define OSTROSLUPN_HH
#include "UkladW.hh"
#include "Inter_rysowania.hh"
#include "Inter_drona.hh"
#include "Inter_krajobrazu.hh"
#include "Przeszkoda.hh"
#include <thread>
#include <chrono>
#include <random>
/**
 * @brief Klasa Wzgorza
 * 
 */
class Wzgorze : public Inter_rysowania, public Inter_krajobrazu, public Przeszkoda
{
    /**
     * @brief Id wzgorza
     * 
     */
    int id;
    /**
     * @brief Promien podstawys
     * 
     */
    double poleR;

public:
/**
 * @brief Construct a new Wzgorze object
 * 
 * @param sr - wektor polozenia figury
 * @param orientacja - macierz rotacji figury
 * @param wysokosc - wysokosc figury
 * @param Min_Max - para przechowujaca minimalna i maksymalna wartosc promienia podtawy figury
 */
    Wzgorze(const Wektor<3> &sr, const MacierzRot<3> &orientacja, const double &wysokosc, std::pair<const double, const double> Min_Max) : Przeszkoda(sr, orientacja, wysokosc, Min_Max), id(-1) {
        this->rysuj();
    };
    /**
     * @brief Metoda rysowania Wzgorza
     * 
     */
    void rysuj() override;
    /**
     * @brief Metoda przeliczajaca wspolrzedne weirzcholkow dla figury
     * 
     * @return std::vector<Wektor<3>> Wektor punktow
     */
    std::vector<Wektor<3>> Przelicz();
    /**
     * @brief Metoda zwracajaca typ figury 
     * 
     * @return std::string - ciag znakow reprezentujacy typ figury ("Wzgorze") 
     */
    std::string typ() override {return "Wzgorze";};
    /**
     * @brief Metoda zwracajaca wektor polozenia figury
     * 
     * @return Wektor<3> - wektor polozenia figury
     */
    Wektor<3> polozenie() override {return this->_srodek;};
    /**
     * @brief Metoda pobierajaca wysokosc figury
     * 
     * @return double - wysokosc figury
     */
    double pobierzWysokosc() override {return this->wysokosc;};
    /**
     * @brief Metoda sprawdzajacaa czy obiekt moze wylodowac na figurze
     * 
     * @param dron - wskaznik na obiekt
     * @param wysokosc - wysokosc na ktorej znajduje sie obiekt
     * @return true - gdy obiekt jest nad figura
     * @return false - gdy obiekt nie jest nad figura
     */
    bool czy_nad(Inter_drona * dron, double wysokosc) override;
    /**
     * @brief Metoda sprawdzajaca czy obiekt moze lodowac na figurze
     * 
     * @param dron - wskaznik na obiekt
     * @param wysokosc - wysokosc na jakiej znajduje sie obiekt
     * @return true - gdy obiekt moze lodowac
     * @return false - gdy obiekt nie moze lodowac
     */
    bool czy_ladowac(Inter_drona * dron, double wysokosc) override;
    /**
     * @brief Destruktor Wzgorza
     * 
     */
    ~Wzgorze() {
        Inter_rysowania::get()->erase_shape(this->id);
        Inter_rysowania::get()->redraw();
    };
};

#endif