#ifndef POWIERZCHNIA_HH
#define POWIERZCHNIA_HH
#include "Inter_rysowania.hh"

/**
 * @brief Klasa powierzchni
 * 
 */
class Powierzchnia : public Inter_rysowania{
        /**
         * @brief Wyskokosc na jakiej znajduje sie powierzchnia
         * 
         */
        double _wysokosc;
    
    public:
        /**
         * @brief Konstruktor powierzchni
         * 
         * @param wysokosc - wysokosc na jakiej ma byc powierzchnia
         */
        Powierzchnia(const double &wysokosc);
        /**
         * @brief Metoda rysowania powierzchni
         * 
         */
        void rysuj() override;
};

#endif