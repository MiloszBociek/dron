#ifndef INTER_KRAJOBRAZU_HH
#define INTER_KRAJOBRAZU_HH


#include "Inter_drona.hh"
#include "Wektor.hh"
#include <iostream>
/**
 * @brief Klasa interfejsu krajobrazu
 * 
 */
class Inter_krajobrazu{

    public:
    /**
     * @brief Wirtualna metoda - sprawdza czy nad obiektem znajduje sie inny obiekt 
     * 
     * @param dron - Obiekt ktory docelowo bedzie nad obiektem krajobrazu
     * @return true - jezeli Dron jest nad obiektem
     * @return false - jezeli Dron nie jest nad obiektem
     */
    virtual bool czy_nad(Inter_drona * dron, double wysokosc) = 0;
    /**
     * @brief Wirtualna metoda - mowi o tym czy mozna ladowac w danym miejscu
     * 
     * @param dron - wskaznik na drona ktory ma ladowac
     * @param wysokosc - wysokosc miejsca ladowania
     * @return true - gdy Dron moze ladowac
     * @return false - gdy Dron nie moze ladowac
     */
    virtual bool czy_ladowac(Inter_drona * dron, double wysokosc) = 0;
    /**
     * @brief Wirtualna metoda - zwraca typ obiektu
     * 
     * @return std::string - zwraca ciag znakow ktory zawiera w sobie nazwe typu obiektu
     */
    virtual std::string typ() = 0;
    /**
     * @brief Wirtualna matoda - zwraca wektor polozenia obiektu
     * 
     * @return Wektor<3> - wektor polozenia obiektu w przestrzeni
     */
    virtual Wektor<3> polozenie() = 0;
    /**
     * @brief Wirtualny destruktor
     * 
     */
    virtual ~Inter_krajobrazu() {};
};
#endif