#ifndef GRANIASTOSLUP6_HH
#define GRANIASTOSLUP6_HH

#include "UkladW.hh"
#include "MacierzRot.hh"
#include "Wektor.hh"
#include "Inter_rysowania.hh"

/**
 * @brief Klasa Graniastoslup6 - uzywana do wirnikow w dronie
 * 
 */
class Graniastoslup6 : public UkladW, public Inter_rysowania {
    /**
     * @brief Promien graniastoslupa
     * 
     */
    double _R;
    /**
     * @brief Wysokosc graniastoslupa
     * 
     */
    double _wysokosc;
    /**
     * @brief Id graniastoslupa
     * 
     */
    int id;
public:
    /**
     * @brief Konstruktor graniastoslupa
     * 
     * @param sr - wektor polozenia graniastoslupa
     * @param orientacja - macierz rotacji dla graniastoslupa
     * @param rodzic - wskaznik na zewnetrzny ukald wspolrzednych 
     * @param R - promien graniastoslupa
     * @param wysokosc - wysokosc graniastoslupa
     */
    Graniastoslup6(const Wektor<3> &sr,const MacierzRot<3> &orientacja, UkladW * rodzic, const double &R, const double &wysokosc) : UkladW(sr, orientacja, rodzic), _R(R), _wysokosc(wysokosc), id(-1) {};
    /**
     * @brief Metoda rysowania graniastoslupa
     * 
     */
    void rysuj() override;
    /**
     * @brief Metoda przelicajaca wspolrzedne dla punktow w graniastoslupie
     * 
     * @return std::array<Wektor<3>, 12> - Wektor wierzcholkow w graniastoslupie
     */
    std::array<Wektor<3>, 12> Przelicz();
    /**
     * @brief Metoda zwracajaca ID graniastoslupa 
     * 
     * @return int - ID graniastoslupa
     */
    int getId(){return id;};
};
#endif