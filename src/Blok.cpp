#include "Blok.hh"
void Blok::rysuj(){
    std::vector<std::vector<drawNS::Point3D>> tmp;
    std::vector<drawNS::Point3D> W;
    int temp;
    
    std::array<Wektor<3>, 8> pom = this->Przelicz();

    for (unsigned int i = 0; i < 8; ++i)
    {
        W.push_back(konwersja(pom[i]));
        if (i % 4 == 3)
        {
            tmp.push_back(W);
            W.clear();
        }
    }
    temp = (Inter_rysowania::get())->draw_polyhedron(tmp, "blue");

    if (id != -1)
    {
        (Inter_rysowania::get())->erase_shape(id);
    }
    id = temp;
    Inter_rysowania::get()->redraw();
}
std::array<Wektor<3>, 8> Blok::Przelicz(){
    std::array<Wektor<3>, 8> punkty;
    int x = 1, y = 1, z = 1;
    for(int i = 0, j = 0; j < 8; ++i, ++j){
        if(i % 2 == 1) y = -y;
        if(i % 3 == 2) x = -x;
        punkty[j] = przelicz_do_globalnego(this->_orientacja * Wektor<3>({x*this->szerokosc/2, y*this->glebokosc/2, z*this->wysokosc/2 + wysokosc/2})); 
        if(i % 4 == 3){
            x = 1;
            y = 1;
            z = -z;
            i = -1;
        }
    }
    return punkty;
}

bool Blok::czy_nad(Inter_drona * dron, double wysokosc){

    Wektor<3> Dystans = dynamic_cast<Inter_krajobrazu*>(dron)->polozenie() - this->polozenie();
    if(std::abs(Dystans[0]) > (this->szerokosc/2 + dron->pobierzR())){return false;};
    if(std::abs(Dystans[1]) > (this->glebokosc/2 + dron->pobierzR())){return false;};

    if(std::abs(Dystans[0]) <= (this->szerokosc/2)) {return true;};
    if(std::abs(Dystans[1]) <= (this->glebokosc/2)) {return true;};

    double Dystans_p = pow((fabs(Dystans[0]) - this->szerokosc/2), 2) + pow(fabs(Dystans[1]) - this->glebokosc/2, 2); 
    return (Dystans_p <= pow(dron->pobierzR(), 2));
}
bool Blok::czy_ladowac(Inter_drona * dron, double wysokosc_d){
    if(wysokosc_d > this->wysokosc){
        Wektor<3> Dystans = dynamic_cast<Inter_krajobrazu*>(dron)->polozenie() - this->polozenie();
        if(std::abs(Dystans[0]) < this->szerokosc/2){return true;};
        if(std::abs(Dystans[1]) < this->glebokosc/2){return true;};

        if(std::abs(Dystans[0]) >= this->szerokosc/2 + dron->pobierzR()) {return false;};
        if(std::abs(Dystans[1]) >= this->glebokosc/2 + dron->pobierzR()) {return false;};
    }
    return false;
}