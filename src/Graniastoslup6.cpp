#include "Graniastoslup6.hh"

void Graniastoslup6::rysuj(){
    std::vector<std::vector<drawNS::Point3D>> tmp;
    std::vector<drawNS::Point3D> W;
    int temp;
    
    std::array<Wektor<3>, 12> pom = this->Przelicz();

    for (unsigned int i = 0; i < 12; ++i)
    {
        W.push_back(konwersja(pom[i]));
        if (i % 6 == 5)
        {
            tmp.push_back(W);
            W.clear();
        }
    }
    temp = Inter_rysowania::get()->draw_polyhedron(tmp, "blue");

    if (id != -1)
    {
        Inter_rysowania::get()->erase_shape(id);
    }
    id = temp;
}

std::array<Wektor<3>, 12> Graniastoslup6::Przelicz(){
    std::array<Wektor<3>, 12> punkty;
    int z = 1;
    for(int i = 0, j = 0; j < 12; ++i, ++j){
        punkty[j] = przelicz_do_globalnego(this->_orientacja * (MacierzRot<3>(60*(i+1), OS::OZ) * Wektor<3>({0, _R, z*_wysokosc/2}))); 
        if(i % 6 == 5){
            z= -z;
            i = -1;
        }
    }
    return punkty;
}