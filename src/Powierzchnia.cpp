#include "Powierzchnia.hh"
Powierzchnia::Powierzchnia(const double & wysokosc){
    this->_wysokosc = wysokosc;
}
void Powierzchnia::rysuj(){
    //std::vector<std::vector<drawNS::Point3D>> Wektor;
    
    for(int i = -10; i < 10; ++i){
        for(int j = -10; j < 10; ++j){
            Inter_rysowania::get()->draw_surface(std::vector<std::vector<drawNS::Point3D> >{{
                drawNS::Point3D(i,j, this->_wysokosc), drawNS::Point3D(i+1, j, this->_wysokosc), drawNS::Point3D(i+1, j, this->_wysokosc), drawNS::Point3D(i, j, this->_wysokosc), drawNS::Point3D(i, j, this->_wysokosc)}, 
                {
                    drawNS::Point3D(i,j+1,this->_wysokosc), drawNS::Point3D(i+1, j+1, this->_wysokosc), drawNS::Point3D(i+1, j+1, this->_wysokosc), drawNS::Point3D(i, j+1, this->_wysokosc), drawNS::Point3D(i, j+1, this->_wysokosc)
                }}, "red");
        }
    }
    Inter_rysowania::get()->redraw();
}