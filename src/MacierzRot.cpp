#include "MacierzRot.hh"

//Metoda mnozenia macierzy przez wektor
template <unsigned int ROZMIAR>
Wektor<ROZMIAR> MacierzRot<ROZMIAR>::operator*(const Wektor<ROZMIAR> &arg2) const
{
  Wektor<ROZMIAR> wynik;

  for (unsigned int i = 0; i < ROZMIAR; ++i)
  {
    wynik[i] = this->macierz[i] * arg2;
  }

  return wynik;
}
//Metoda transpozycji macierzy (przydatna do mnozenia macierzy)
template <unsigned int ROZMIAR>
MacierzRot<ROZMIAR> MacierzRot<ROZMIAR>::transpozycja() const
{
  MacierzRot<ROZMIAR> Trans;
  for (unsigned int i = 0; i < ROZMIAR; ++i)
  {
    for (unsigned int j = 0; j < ROZMIAR; ++j)
    {
      Trans[i][j] = macierz[j][i];
    }
  }
  return Trans;
}
//Metoda mnozenia macierzy przez macierz
template <unsigned int ROZMIAR>
MacierzRot<ROZMIAR> MacierzRot<ROZMIAR>::operator*(const MacierzRot<ROZMIAR> &arg2) const
{

  MacierzRot<ROZMIAR> macierz_w;
  MacierzRot<ROZMIAR> transponowana;
  transponowana = arg2.transpozycja();

  for (unsigned int i = 0; i < ROZMIAR; ++i)
  {
    for (unsigned int j = 0; j < ROZMIAR; ++j)
    {
      macierz_w[i][j] = macierz[i] * transponowana[j];
    }
  }
  return macierz_w;
}
//Getter
template <unsigned int ROZMIAR>
const Wektor<ROZMIAR> &MacierzRot<ROZMIAR>::operator[](unsigned int indeks) const
{
  if (indeks < 0 || indeks > ROZMIAR)
  {
    std::cerr << "Niepoprawny indeks" << std::endl;
    exit(0);
  }
  return macierz[indeks];
}
//Setter
template <unsigned int ROZMIAR>
Wektor<ROZMIAR> &MacierzRot<ROZMIAR>::operator[](unsigned int indeks)
{
  if (indeks < 0 || indeks > ROZMIAR)
  {
    std::cerr << "Niepoprawny indeks" << std::endl;
    exit(0);
  }
  return macierz[indeks];
}
//Przeciazenei operatora przypisywania
template <unsigned int ROZMIAR>
MacierzRot<ROZMIAR> &MacierzRot<ROZMIAR>::operator = (const MacierzRot<ROZMIAR> &mac){
  for(unsigned int i = 0; i < ROZMIAR; ++i){
    for(unsigned int j = 0; j < ROZMIAR; ++j){
      macierz[i][j] = mac[i][j];
    }
  }
  return *this;
}
//Konstruktor macierzy jedynkowej
template <unsigned int ROZMIAR>
MacierzRot<ROZMIAR>::MacierzRot()
{
  for (unsigned int i = 0; i < ROZMIAR; ++i)
  {
    for (unsigned int j = 0; j < ROZMIAR; ++j)
    {
      if (i == j)
      {
        this->macierz[i][j] = 1;
      }
      else
      {
        this->macierz[i][j] = 0;
      }
    }
  }
}
//Konsturktor macierzy rotacji >3D
template <unsigned int ROZMIAR>
MacierzRot<ROZMIAR>::MacierzRot(const double &kat_stopnie, OS o)
{

  static_assert(ROZMIAR < 4, "Nie wykonujemy hiperobrotow!");
}
//Konsturktor macierzy rotacji 2D
template <>
MacierzRot<2>::MacierzRot(const double &kat_stopnie, OS o)
{

  macierz[0][0] = cos(kat_stopnie * M_PI / 180);
  macierz[0][1] = -sin(kat_stopnie * M_PI / 180);
  macierz[1][0] = sin(kat_stopnie * M_PI / 180);
  macierz[1][1] = cos(kat_stopnie * M_PI / 180);
}
//Konsturktor macierzy rotacji 3D
template <>
MacierzRot<3>::MacierzRot(const double &kat, OS o)
{

  switch (o)
  {
  case OX:
    macierz[0][0] = 1;
    macierz[0][1] = 0;
    macierz[0][2] = 0;
    macierz[1][0] = 0;
    macierz[1][1] = cos(kat * M_PI / 180);
    macierz[1][2] = -sin(kat * M_PI / 180);
    macierz[2][0] = 0;
    macierz[2][1] = sin(kat * M_PI / 180);
    macierz[2][2] = cos(kat * M_PI / 180);
    break;
  case OY:
    macierz[0][0] = cos(kat * M_PI / 180);
    macierz[0][1] = 0;
    macierz[0][2] = sin(kat * M_PI / 180);
    macierz[1][0] = 0;
    macierz[1][1] = 1;
    macierz[1][2] = 0;
    macierz[2][0] = -sin(kat * M_PI / 180);
    macierz[2][1] = 0;
    macierz[2][2] = cos(kat * M_PI / 180);
    break;
  case OZ:
    macierz[0][0] = cos(kat * M_PI / 180);
    macierz[0][1] = -sin(kat * M_PI / 180);
    macierz[0][2] = 0;
    macierz[1][0] = sin(kat * M_PI / 180);
    macierz[1][1] = cos(kat * M_PI / 180);
    macierz[1][2] = 0;
    macierz[2][0] = 0;
    macierz[2][1] = 0;
    macierz[2][2] = 1;
    break;
  }
}

//Przeciazenie operatora do wypisywania
template <unsigned int ROZMIAR>
std::ostream &operator<<(std::ostream &Strm, const MacierzRot<ROZMIAR> &macierz)
{
  for (unsigned int i = 0; i < ROZMIAR; ++i)
  {
    Strm << macierz[i] << std::endl;
  }
  return Strm;
}

//Przeciazenie operatora do wczytywania enum
std::istream &operator>>(std::istream &Strm, OS &os)
{
  char tmp_os;
  Strm >> tmp_os;
  switch (tmp_os)
  {
  case 'x':
  case 'X':
  {
    os = OX;
    break;
  }
  case 'y':
  case 'Y':
  {
    os = OY;
    break;
  }
  case 'z':
  case 'Z':
  {
    os = OZ;
    break;
  }
  default:
  {
    Strm.setstate(std::ios::failbit);
    break;
  }
  }
  return Strm;
}


template class MacierzRot<3>;
template std::ostream &operator<<(std::ostream &Strm, const MacierzRot<2> &macierz);
template std::ostream &operator<<(std::ostream &Strm, const MacierzRot<3> &macierz);
