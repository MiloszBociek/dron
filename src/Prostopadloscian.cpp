#include "Prostopadloscian.hh"

void Prostopadloscian::rysuj(){
    std::vector<std::vector<drawNS::Point3D>> tmp;
    std::vector<drawNS::Point3D> W;
    int temp;
    
    std::array<Wektor<3>, 8> pom = this->Przelicz();

    for (unsigned int i = 0; i < 8; ++i)
    {
        W.push_back(konwersja(pom[i]));
        if (i % 4 == 3)
        {
            tmp.push_back(W);
            W.clear();
        }
    }
    temp = (Inter_rysowania::get())->draw_polyhedron(tmp, "blue");

    if (id != -1)
    {
        (Inter_rysowania::get())->erase_shape(id);
    }
    id = temp;
}

std::array<Wektor<3>, 8> Prostopadloscian::Przelicz(){
    std::array<Wektor<3>, 8> punkty;
    int x = 1, y = 1, z = 1;
    for(int i = 0, j = 0; j < 8; ++i, ++j){
        if(i % 2 == 1) y = -y;
        if(i % 3 == 2) x = -x;
        punkty[j] = przelicz_do_globalnego(this->_orientacja * Wektor<3>({x*_szerokosc/2, y*_dlugosc/2, z*_wysokosc/2})); 
        if(i % 4 == 3){
            x = 1;
            y = 1;
            z = -z;
            i = -1;
        }
    }
    return punkty;
}
