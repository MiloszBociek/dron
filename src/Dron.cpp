#include "Dron.hh"
#include <vector>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <limits>
#include <fstream>
#include "Scena.hh"


void Dron::obroc_wirniki(const double & predkosc)
{
    double v = predkosc;
    for (int i = 0; i < 4; ++i)
    {
        for (int j = 0; j < 2; ++j)
        {
            if (i == 0 || i == 3)
            {
                this->smigla[i][j].Rotacja(MacierzRot<3>(-v, OS::OZ));
            }
            else
            {
                this->smigla[i][j].Rotacja(MacierzRot<3>(v, OS::OZ));
            }
        }
    }
    v = -v;
}

void Dron::lec_w_gore(const double &ile)
{
    Wektor<3> wektor({0, 0, ile});
    this->_srodek = this->_srodek + wektor;
}

void Dron::lec_w_przod(const double &ile)
{
    Wektor<3> wektor({ile, 0, 0});
    this->_srodek = (this->_srodek) + (this->_orientacja * wektor);
}

void Dron::obroc(const double &kat)
{
    MacierzRot<3> macierz(kat, OS::OZ);
    this->_orientacja = this->_orientacja * macierz;
}

void Dron::rysuj()
{
    korpus.rysuj();
    for (int i = 0; i < 4; ++i)
    {
        wirniki[i].rysuj();
    }
    for (int i = 0; i < 4; ++i)
    {
        for (int j = 0; j < 2; ++j)
        {
            smigla[i][j].rysuj();
        }
    }
    Inter_rysowania::get()->redraw();
}

void Dron::animacja(const double &wysokosc, const double &odleglosc, const double &kat)
{
    int szybkosc_animacji = 10;
    int znak_odleglosci = 1;
    int id_l;
    static double wysokosc_przeszkody = 0;
    double wysokosc_drona = 0;
    bool ladowanie = false, ladowanie_2 = false, nad_przeszkoda = false, pobierz_wysokosc_drona = true;
    
    for (int i = 0; i < szybkosc_animacji; ++i)
    {
        this->lec_w_gore(wysokosc / szybkosc_animacji);
        this->obroc_wirniki(4);
        this->rysuj();
        Inter_rysowania::get()->redraw();
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }
    for (int i = 0; i < szybkosc_animacji; ++i)
    {

        this->obroc(kat / szybkosc_animacji);
        this->obroc_wirniki(4);
        this->rysuj();
        Inter_rysowania::get()->redraw();
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }
    for (int i = 0; i < szybkosc_animacji; ++i)
    {
        for(unsigned long int i = 0; i < Scena::pobierzListe().size(); ++i){
            if(std::dynamic_pointer_cast<Dron>(Scena::pobierzListe()[i]).get() != this){
                if(Scena::pobierzListe()[i]->czy_nad(this, this->polozenie()[2]) && 
                this->polozenie()[2] < std::dynamic_pointer_cast<Przeszkoda>(Scena::pobierzListe()[i])->pobierzWysokosc()){
                    znak_odleglosci = -znak_odleglosci;
                }
            }
        }
        this->lec_w_przod(znak_odleglosci*odleglosc / szybkosc_animacji);
        this->obroc_wirniki(4);
        this->rysuj();
        Inter_rysowania::get()->redraw();
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }

    for (int k = 0; k < szybkosc_animacji; ++k)
    {
        for(unsigned long int i = 0; i < Scena::pobierzListe().size(); ++i){
            if(std::dynamic_pointer_cast<Dron>(Scena::pobierzListe()[i]).get() != this){
                if(Scena::pobierzListe()[i]->czy_nad(this, this->polozenie()[2])){
                    nad_przeszkoda = true;
                    id_l = i;
                    if(Scena::pobierzListe()[i]->czy_ladowac(this, this->polozenie()[2])){
                        ladowanie = true;
                        wysokosc_przeszkody = std::dynamic_pointer_cast<Przeszkoda>(Scena::pobierzListe()[i])->pobierzWysokosc();
                        //break;
                    }else{
                        ladowanie_2 = true;
                    }
                }
            }
        }
        if(ladowanie && nad_przeszkoda && !ladowanie_2){
            if(pobierz_wysokosc_drona){
                wysokosc_drona = this->polozenie()[2];
                pobierz_wysokosc_drona = false;
            }
            this->lec_w_gore(-(wysokosc_drona - wysokosc_przeszkody) / szybkosc_animacji);
            this->obroc_wirniki(4);
            this->rysuj();
            Inter_rysowania::get()->redraw();
            std::this_thread::sleep_for(std::chrono::milliseconds(1));
        }else if((!ladowanie && nad_przeszkoda) || ladowanie_2){
            this->lec_w_przod(odleglosc / szybkosc_animacji);
            while(nad_przeszkoda){
                this->lec_w_przod(odleglosc / szybkosc_animacji);
                this->obroc_wirniki(4);
                this->rysuj();
                Inter_rysowania::get()->redraw();
                std::this_thread::sleep_for(std::chrono::milliseconds(1));
                if(!Scena::pobierzListe()[id_l]->czy_nad(this, this->polozenie()[2])){
                    nad_przeszkoda = false;
                    break;
                }
            }
            ladowanie_2 = false;
            if(pobierz_wysokosc_drona){
                wysokosc_drona = this->polozenie()[2];
                pobierz_wysokosc_drona = false;
            }
            for(int l = 0; l < szybkosc_animacji; ++l){
                    this->lec_w_gore(-wysokosc_drona / szybkosc_animacji);
                    this->obroc_wirniki(4);
                    this->rysuj();
                    Inter_rysowania::get()->redraw();
                    std::this_thread::sleep_for(std::chrono::milliseconds(1));
                }
                break;
        }else if(!ladowanie && !nad_przeszkoda){
            if(pobierz_wysokosc_drona){
                wysokosc_drona = this->polozenie()[2];
                pobierz_wysokosc_drona = false;
            }
            for(int l = 0; l < szybkosc_animacji; ++l){
                this->lec_w_gore(-wysokosc_drona / szybkosc_animacji);
                this->obroc_wirniki(4);
                this->rysuj();
                Inter_rysowania::get()->redraw();
                std::this_thread::sleep_for(std::chrono::milliseconds(1));
            }
            break;
        }
    }
}
/**
 * @brief Metoda pobierajaca promien drona
 * 
 * @return double 
 */
double Dron::pobierzR(){
    return (std::sqrt(std::pow(dszerokosc / 2, 2) + std::pow(dglebokosc / 2, 2)) + 0.1);
}

bool Dron::czy_nad(Inter_drona * dron, double wysokosc_d){
    if(wysokosc_d > this->polozenie()[2]){
        Wektor<3> Dystans = dynamic_cast<Inter_krajobrazu*>(dron)->polozenie() - this->polozenie();
        if(std::sqrt(pow(Dystans[0], 2) + pow(Dystans[1], 2)) <= (poleR + dron->pobierzR())){
            return true;
        }
    }
    return false;
}

bool Dron::czy_ladowac(Inter_drona * dron,  double wysokosc_d){
    return false;
}

Dron::~Dron(){
    Inter_rysowania::get()->erase_shape(korpus.getId());
    for(unsigned int i = 0; i < 4; ++i){
        for(unsigned int j = 0; j < 2; ++j){
            Inter_rysowania::get()->erase_shape(smigla[i][j].getId());
        }
    }
    for(unsigned int i = 0; i < 4; ++i){
        Inter_rysowania::get()->erase_shape(wirniki[i].getId());
    }
    Inter_rysowania::get()->erase_shape(this->id);
    Inter_rysowania::get()->redraw();
}