
#include "Scena.hh"

using namespace std;

int main(int argc, char **argv)
{
  Scena S;
  char znak;
  double wysokosc, odleglosc, kat_stopnie;

  
  S.wypisz_wszystko();

  while (true)
  {
    cout << "Menu" << endl;
    cout << "t - lot dronem" << endl;
    cout << "w - ile jest wektorow" << endl;
    cout << "m - menu" << endl;
    cout << "d - dodaj element krajobrazu" << endl;
    cout << "u - usun obiekt" << endl;
    cout << "k - koniec" << endl;

    cin >> znak;

    switch (znak)
    {
    case 't':
    {
      unsigned int id;
      system("clear");
      cout << "Podaj id drona" << endl;
      cin >> id;
      cout << "Podaj wysokość przelotowa" << endl;
      cin >> wysokosc;
      cout << "Podaj odleglosc" << endl;
      cin >> odleglosc;
      cout << "Podaj kat obrotu w stopniach" << endl;
      cin >> kat_stopnie;
      if (!cin.good())
      {
        cout << "Zle parametry!" << endl;
        cin.clear();
        cin.ignore(numeric_limits<int>::max(), '\n');
      }
      else
      {
        S.animuj(wysokosc, odleglosc, kat_stopnie, id-1);
      }
      break;
    }
    case 'w':
    {
      system("clear");
      cout << "Ilosc wektorow: " << endl;
      cout << "Obecnie: " << Wektor<3>::ile_jest() << endl;
      cout << "Ogolnie stworzono: " << Wektor<3>::ile_stworzono() << endl;
      cout << "---------------------------------" << endl;
      break;
    }
    case 'D':
    case 'd':{
      system("clear");
      S.dodaj();
      break;

    }
    case 'U':
    case 'u' :{
      system("clear");
      S.usun();
      break;

    }
    case 'm':
    {
      system("clear");
      break;
    }
    case 'k':
    {
      exit(0);
      break;
    }
    default:
    {
      system("clear");
      cout << "Nie ma takiej opcji!" << endl << endl;
      break;
    }
    }
  }
}
