#include "Wzgorze.hh"
#include <cstdlib>
#include <random>
#include <ctime>

//void OstroslupN::rysuj(drawNS::Draw3DAPI *rysownik){};

void Wzgorze::rysuj(){
    std::vector<std::vector<drawNS::Point3D>> tmp;
    std::vector<drawNS::Point3D> W;
    int temp;
    
    std::vector<Wektor<3> > pom = this->Przelicz();

    for (int i = 0; i < this->wierzcholki*2; ++i)
    {
        W.push_back(konwersja(pom[i]));
        if (i % this->wierzcholki == this->wierzcholki - 1)
        {
            tmp.push_back(W);
            W.clear();
        }
    }
    temp = Inter_rysowania::get()->draw_polyhedron(tmp, "blue");

    if (id != -1)
    {
        Inter_rysowania::get()->erase_shape(id);
    }
    id = temp;
    Inter_rysowania::get()->redraw();
}

bool Wzgorze::czy_nad(Inter_drona* dron, double wysokosc){
    Wektor<3> Dystans = dynamic_cast<Inter_krajobrazu*>(dron)->polozenie() - this->polozenie();
    /*this->polozenie().dlugosc() - dynamic_cast<Inter_krajobrazu*>(dron)->polozenie().dlugosc()*/ 
    if(std::sqrt(pow(Dystans[0], 2) + pow(Dystans[1], 2))  <= poleR + dron->pobierzR()){
        return true;
    }
    else{
        return false;
    }
}

bool Wzgorze::czy_ladowac(Inter_drona* dron, double wysokosc){
return false;
}

std::vector<Wektor<3> > Wzgorze::Przelicz(){
    Wektor<3> punkt;
    std::vector<Wektor<3> > punkty;
    double promien, max = 0;
    for(int i = 0; i < this->wierzcholki; ++i){
        promien = this->losuj(min_max.first, min_max.second);
        punkt = przelicz_do_globalnego((MacierzRot<3>((360/this->wierzcholki)*(i+1), OS::OZ) * Wektor<3>({0, promien, 0}))); 
        punkty.push_back(punkt);
        if(promien > max) max = promien;
    }
    for(int i = 0; i < this->wierzcholki; ++i){
        punkt = przelicz_do_globalnego(Wektor<3>({0, 0, this->wysokosc})); 
        punkty.push_back(punkt);
    }
    poleR = max;
    return punkty;
}