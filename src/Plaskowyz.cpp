#include "Plaskowyz.hh"
#include <cstdlib>
#include <random>
#include <ctime>

void Plaskowyz::rysuj(){
    
    std::vector<std::vector<drawNS::Point3D>> tmp;
    std::vector<drawNS::Point3D> W;
    int temp;
    
    std::vector<Wektor<3> > pom = this->Przelicz();

    for (int i = 0; i < this->wierzcholki*2; ++i)
    {
        W.push_back(konwersja(pom[i]));
        if (i % this->wierzcholki == this->wierzcholki - 1)
        {
            tmp.push_back(W);
            W.clear();
        }
    }
    temp = Inter_rysowania::get()->draw_polyhedron(tmp, "blue");

    if (id != -1)
    {
        Inter_rysowania::get()->erase_shape(id);
    }
    id = temp;
    Inter_rysowania::get()->redraw();

}

std::vector<Wektor<3> > Plaskowyz::Przelicz(){
    double wys = 0;
    Wektor<3> punkt;
    std::vector<Wektor<3> > punkty;
    double Promien, max= 0, min = min_max.second;
    for(int i = 0; i < this->wierzcholki*2; ++i){
        Promien = this->losuj(min_max.first, min_max.second);
        punkt = przelicz_do_globalnego((MacierzRot<3>((360/this->wierzcholki)*(i+1), OS::OZ) * Wektor<3>({0, Promien, wys}))); 
        punkty.push_back(punkt);  
        if(Promien > max){
            max = Promien;
        }
        if(Promien < min && i > this->wierzcholki){
            min = Promien;
        }
        if(i % this->wierzcholki == this->wierzcholki - 1){
            wys = this->wysokosc;
        }
    }
    R_max = max;
    Rg_min = min;
    return punkty;
}

bool Plaskowyz::czy_nad(Inter_drona* dron, double wysokosc){

    Wektor<3> Dystans = dynamic_cast<Inter_krajobrazu*>(dron)->polozenie() - this->polozenie();
    if (std::sqrt(pow(Dystans[0], 2) + pow(Dystans[1], 2)) <= R_max + dron->pobierzR()){
        return true;
    }
    return false;
}

bool Plaskowyz::czy_ladowac(Inter_drona* dron, double wysokosc_d){
    if(wysokosc_d > this->wysokosc){
        Wektor<3> Dystans = dynamic_cast<Inter_krajobrazu*>(dron)->polozenie() - this->polozenie();
        if(std::sqrt(pow(Dystans[0], 2) + pow(Dystans[1], 2)) < this->Rg_min - dron->pobierzR()/2){
            return true;
        }
    }
    return false;
}