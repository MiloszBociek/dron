#include "Przeszkoda.hh"

double Przeszkoda::losuj(double Min, double Max){
    static std::default_random_engine generator(std::chrono::steady_clock::now().time_since_epoch().count());
    std::uniform_real_distribution<double> distribution(Min,Max);
    double wartosc = distribution(generator);
    return wartosc;
}