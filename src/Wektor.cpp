#include "Wektor.hh"
#include <array>

//Getter
template <unsigned int ROZMIAR>
const double & Wektor<ROZMIAR>::operator [] (unsigned int indeks) const {
    if(indeks < 0 || indeks > ROZMIAR){
      std::cerr << "Niepoprawny indeks" << std::endl;
      exit(0);
    }
    return wektor[indeks];
    }


//Setter
template <unsigned int ROZMIAR>
  double & Wektor<ROZMIAR>::operator [] (unsigned int indeks) { 
    if(indeks < 0 || indeks > ROZMIAR){
      std::cerr << "Niepoprawny indeks" << std::endl;
      exit(0);
    }
    return wektor[indeks];
  }

template<unsigned int ROZMIAR>
  Wektor<ROZMIAR> Wektor<ROZMIAR>::operator = (const Wektor<ROZMIAR> & arg2){
      for(unsigned int i = 0; i < ROZMIAR; ++i){
         this->wektor[i] = arg2[i];
      }
    return this->wektor;
  }


//Dodawanie wektorowe
template <unsigned int ROZMIAR>
Wektor<ROZMIAR> Wektor<ROZMIAR>::operator + (const Wektor<ROZMIAR> & arg2) const {

    Wektor<ROZMIAR> wynik;

    for(unsigned int i = 0; i< ROZMIAR; ++i){
        wynik[i] = this->wektor[i] + arg2[i];
    }
    

    return wynik;
}

//Odejmowanie wektorowe
template <unsigned int ROZMIAR>
Wektor<ROZMIAR> Wektor<ROZMIAR>::operator - (const Wektor<ROZMIAR> & arg2) const {

    Wektor<ROZMIAR> wynik;

    for(unsigned int i = 0; i< ROZMIAR; ++i){
        wynik[i] = this->wektor[i] - arg2[i];
    }
    

    return wynik;
}

//Mnozenie wektora i liczby
template <unsigned int ROZMIAR>
Wektor<ROZMIAR> Wektor<ROZMIAR>::operator * (const double & arg2) const {

    Wektor<ROZMIAR> wynik;

    for(unsigned int i = 0; i< ROZMIAR; ++i){
        wynik[i] = this->wektor[i] * arg2;
    }
    return wynik;
}

//Iloczyn skalarny
template <unsigned int ROZMIAR>
double Wektor<ROZMIAR>::operator * (const Wektor<ROZMIAR> & arg2) const {

    double wynik = 0;

    for(unsigned int i = 0; i < ROZMIAR; ++i){
        wynik += this->wektor[i]*arg2[i];
    }
    return wynik;
}

//Obliczanie dlugosci wektora
template <unsigned int ROZMIAR>
double Wektor<ROZMIAR>::dlugosc() const {

    double wynik = 0;

    for(unsigned int i = 0; i < ROZMIAR; ++i){
        wynik += pow(this->wektor[i], 2);
    }
    return sqrt(wynik);
}

//Operator wczytania wektora ze strumienia
template <unsigned int ROZMIAR>
std::istream & operator >> (std::istream & strm, Wektor<ROZMIAR> & wektor){
    for(unsigned int i = 0; i < ROZMIAR; ++i){
        
        strm>>wektor[i];

        if(!strm.good()){
            strm.clear();
            strm.ignore(std::numeric_limits<int>::max(), '\n');
            std::cerr << "Blad danych"<<std::endl;
            exit(0);
        }
    }

    return strm;
}

//Operator wypisania wektora na strumien
template <unsigned int ROZMIAR>
std::ostream& operator << (std::ostream & strm, const Wektor<ROZMIAR> & wektor){
    strm << ' ';
    for(unsigned int i = 0; i < ROZMIAR; ++i){
        strm<<wektor[i];
        strm << ' ';
    }
    return strm;
}

template class Wektor<2>;
template std::ostream& operator << (std::ostream & strm, const Wektor<2> & wektor);
template std::istream & operator >> (std::istream & strm, Wektor<2> & wektor);

template class Wektor<3>;
template std::ostream& operator << (std::ostream & strm, const Wektor<3> & wektor);
template std::istream & operator >> (std::istream & strm, Wektor<3> & wektor);

template class Wektor<6>;
template std::ostream& operator << (std::ostream & strm, const Wektor<6> & wektor);
template std::istream & operator >> (std::istream & strm, Wektor<6> & wektor);