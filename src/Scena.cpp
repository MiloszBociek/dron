#include "Scena.hh"

std::vector<std::shared_ptr<Inter_krajobrazu>> Scena::lista_obiektow;

Scena::Scena()
{

  Powierzchnia podloga(0);
  podloga.rysuj();

  lista_obiektow.push_back(std::dynamic_pointer_cast<Inter_krajobrazu>(std::shared_ptr<Dron>(new Dron(Wektor<3>({0, 0, 0}), MacierzRot<3>()))));
 
  lista_obiektow.push_back(std::dynamic_pointer_cast<Inter_krajobrazu>(std::shared_ptr<Blok>(new Blok(Wektor<3>({5, -5, 0}), MacierzRot<3>(), 5, 3, 3))));
  
  lista_obiektow.push_back(std::dynamic_pointer_cast<Inter_krajobrazu>(std::shared_ptr<Wzgorze>(new Wzgorze(Wektor<3>({5, 5, 0}), MacierzRot<3>(), 10, {2, 3}))));
  
  lista_obiektow.push_back(std::dynamic_pointer_cast<Inter_krajobrazu>(std::shared_ptr<Plaskowyz>(new Plaskowyz(Wektor<3>({-5, -5, 0}), MacierzRot<3>(), 5, {2, 3}))));

  lista_obiektow.push_back(std::dynamic_pointer_cast<Inter_krajobrazu>(std::shared_ptr<GraniastoslupPrawidlowy>(new GraniastoslupPrawidlowy(Wektor<3>({-5, 5, 0}), MacierzRot<3>(), 10, {2, 3}))));

}

void Scena::animuj(double wysokosc, double odleglosc, double kat_stopnie, unsigned int id)
{
  if (this->znajdzIdDrona(id) == -1)
  {
    std::cout << "Podane id nie istnieje" << std::endl;
    std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    system("clear");
  }
  else
  {
    std::dynamic_pointer_cast<Inter_drona>(lista_obiektow[this->znajdzIdDrona(id)])->animacja(wysokosc, odleglosc, kat_stopnie);
  }
}

void Scena::dodaj()
{

  char wybor_dodaj;
  Wektor<3> polozenie;
  double wysokosc, szerokosc, glebokosc, min, max;

  while (true)
  {

    std::cout << "Jaki rodzaj przeszkody dodać?" << std::endl;
    std::cout << "w - wzgorze" << std::endl;
    std::cout << "p - plaskowyz" << std::endl;
    std::cout << "g - plaskowyz prawidlowy" << std::endl;
    std::cout << "b - blok" << std::endl;
    std::cout << "d - dron" << std::endl;
    std::cout << "k - powrot do menu" << std::endl;

    std::cin >> wybor_dodaj;

    if (!std::cin.good())
    {
      std::cout << "Zle parametry!" << std::endl;
      std::cin.clear();
      std::cin.ignore(std::numeric_limits<int>::max(), '\n');
      system("clear");
    }
    switch (wybor_dodaj)
    {

    case 'W':
    case 'w':
    {
      std::cout << "Podaj wektor polozenia: ";
      std::cin >> polozenie;
      std::cout << std::endl
                << "Podaj wysokosc wzgorza: ";
      std::cin >> wysokosc;
      std::cout << std::endl
                << "Podaj wartosc minimalna i maksymalna dla promienia ";
      std::cin >> min >> max;
      if (!std::cin.good() || max < min || min < 0 || max < 0)
      {
        std::cout << "Zle parametry!" << std::endl;
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<int>::max(), '\n');
      }
      else
      {
        lista_obiektow.push_back(std::dynamic_pointer_cast<Inter_krajobrazu>(std::shared_ptr<Wzgorze>(new Wzgorze(polozenie, MacierzRot<3>(), wysokosc, {min, max}))));
        system("clear");
      }

      break;
    }

    case 'P':
    case 'p':
    {
      std::cout << "Podaj wektor polozenia: ";
      std::cin >> polozenie;
      std::cout << std::endl
                << "Podaj wysokoc plaskowyzu: ";
      std::cin >> wysokosc;
      std::cout << std::endl
                << "Podaj wartosc minimalna i maksymalna dla promienia ";
      std::cin >> min >> max;
      if (!std::cin.good() || max < min || min < 0 || max < 0)
      {
        std::cout << "Zle parametry!" << std::endl;
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<int>::max(), '\n');
      }
      else
      {

        lista_obiektow.push_back(std::dynamic_pointer_cast<Inter_krajobrazu>(std::shared_ptr<Plaskowyz>(new Plaskowyz(polozenie, MacierzRot<3>(), wysokosc, {min, max}))));
        system("clear");
      }
      break;
    }
    case 'G':
    case 'g':
    {
      std::cout << "Podaj wektor polozenia: ";
      std::cin >> polozenie;
      std::cout << std::endl
                << "Podaj wysokoc plaskowyzu: ";
      std::cin >> wysokosc;
      std::cout << std::endl
                << "Podaj wartosc minimalna i maksymalna dla promienia ";
      std::cin >> min >> max;
      if (!std::cin.good() || max < min || min < 0 || max < 0)
      {
        std::cout << "Zle parametry!" << std::endl;
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<int>::max(), '\n');
      }
      else
      {

        lista_obiektow.push_back(std::dynamic_pointer_cast<Inter_krajobrazu>(std::shared_ptr<GraniastoslupPrawidlowy>(new GraniastoslupPrawidlowy(polozenie, MacierzRot<3>(), wysokosc, {min, max}))));
        system("clear");
      }
      break;
    }
    case 'B':
    case 'b':{
      std::cout << "Podaj wektor polozenia: ";
      std::cin >> polozenie;
      std::cout << "Podaj wysokosc bloku: ";
      std::cin >> wysokosc;
      std::cout << "Podaj szerokosc bloku: ";
      std::cin >> szerokosc;
      std::cout << "Podaj glebokosc bloku: ";
      std::cin >> glebokosc;
      if (!std::cin.good())
      {
        std::cout << "Zle parametry!" << std::endl;
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<int>::max(), '\n');
      }
      else
      {

        lista_obiektow.push_back(std::dynamic_pointer_cast<Inter_krajobrazu>(std::shared_ptr<Blok>(new Blok(polozenie, MacierzRot<3>(), wysokosc, szerokosc, glebokosc))));
        system("clear");
      }
      break;
    }
    case 'D':
    case 'd':
    {
      std::cout << "Podaj polozenie nowego drona" << std::endl;
      std::cin >> polozenie;
      if (!std::cin.good())
      {
        std::cout << "Zle parametry!" << std::endl;
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<int>::max(), '\n');
      }
      else
      {
        lista_obiektow.push_back(std::dynamic_pointer_cast<Inter_krajobrazu>(std::shared_ptr<Dron>(new Dron(polozenie, MacierzRot<3>()))));
        system("clear");
      }
      break;
    }
    case 'K':
    case 'k':
    {
      system("clear");
      return;
    }

    default:
      system("clear");
      std::cout << "Nie ma takiej opcji!" << std::endl;
      break;
    }
  }
}

void Scena::usun()
{
  char wybor_usun;
  unsigned int id;

  while (true)
  {

    std::cout << "w - wyswietl liste obiektow " << std::endl;
    std::cout << "d - usun drona " << std::endl;
    std::cout << "e - usun element krajobrazu " << std::endl;
    std::cout << "k - powrot do menu" << std::endl;
    std::cin >> wybor_usun;

    if (!std::cin.good())
    {
      std::cout << "Zle parametry!" << std::endl;
      std::cin.clear();
      std::cin.ignore(std::numeric_limits<int>::max(), '\n');
    }
    switch (wybor_usun)
    {

    case 'W':
    case 'w':
    {
      int usuwanie = 0;
      system("clear");
      std::cout << "Wybierz typ obiektu ktory chcesz usunac" << std::endl;
      std::cout << "1. Dron" << std::endl;
      std::cout << "2. Przeszkoda" << std::endl;
      std::cin >> usuwanie;
      switch (usuwanie)
      {
      case 1:
      {
        system("clear");
        this->wypisz_drony();
        std::cout<<"--------------------------------"<<std::endl;
        break;
      }
      case 2:
      {
        system("clear");
        this->wypisz_przeszkody();
        std::cout<<"--------------------------------"<<std::endl;
        break;
      }
      default:
      {
        std::cout << "Nie ma takiej opcji!" << std::endl;
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
        system("clear");
        break;
      }
      }
      break;
    }
    case 'D':
    case 'd':
    {
      std::cout << "Podaj ID drona: ";
      std::cin >> id;
      std::cout << std::endl;
      if (std::cin.good() && this->znajdzIdDrona(id - 1) != -1)
      {
        std::cout << "USUWANIE DRONA" << std::endl;
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
        system("clear");
        this->usunDrona(id - 1);
        
      }
      else
      {
        std::cout << "Dron o podanym ID nie istnieje!" << std::endl;
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
        system("clear");
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<int>::max(), '\n');
      }
      break;
    }

    case 'E':
    case 'e':
    {
      std::cout << "Podaj ID Przeszkody: ";
      std::cin >> id;
      std::cout << std::endl;
      if (std::cin.good() && this->znajdzIdPrzeszkody(id - 1) != -1)
      {
        std::cout << "USUWANIE ELEMENTU" << std::endl;
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
        system("clear");
        this->usunPrzeszkode(id - 1);
      }
      else
      {
        std::cout << "Element o podanym ID nie istnieje!" << std::endl;
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<int>::max(), '\n');
      }
      break;
    }
    case 'K':
    case 'k':{
      system("clear");
      return;
    }
    default:
    {
      system("clear");
      std::cout << "Nie ma takiej opcji" <<::std::endl;
    }
    }
  }
  // std::shared_ptr<Inter_krajobrazu> mt = lista_obiektow[id];
  // std::shared_ptr<Inter_rysowania> rys = std::reinterpret_pointer_cast<Inter_rysowania>(mt);
  // lista_rys.erase(std::remove(lista_obiektow.begin(), lista_obiektow.end(), rys), lista_obiektow.end());
  //lista_obiektow.erase();
}
void Scena::wypisz_wszystko() const
{
  std::cout << "TYP   ID  POLOZENIE" << std::endl;
  for (unsigned int i = 0; i < lista_obiektow.size(); ++i)
  {
    std::cout << lista_obiektow[i]->typ() << "  " << i + 1 << "  (" << lista_obiektow[i]->polozenie() << ")" << std::endl;
  }
}

void Scena::wypisz_drony() const
{
  std::cout << "TYP   ID  POLOZENIE" << std::endl;
  unsigned int i = 0, j = 0;
  for (std::shared_ptr<Inter_krajobrazu> element : lista_obiektow)
  {
    if (std::dynamic_pointer_cast<Inter_drona>(element) != nullptr)
    {
      std::cout << lista_obiektow[i]->typ() << "  " << j + 1 << "  (" << lista_obiektow[i]->polozenie() << ")" << std::endl;
      j++;
    }
    i++;
  }
}

void Scena::wypisz_przeszkody() const
{
  std::cout << "TYP   ID    POLOZENIE" << std::endl;
  unsigned int i = 0, j = 0;
  for (std::shared_ptr<Inter_krajobrazu> element : lista_obiektow)
  {
    if (std::dynamic_pointer_cast<Przeszkoda>(element) != nullptr)
    {
      std::cout << lista_obiektow[i]->typ() << "  " << j + 1 << "  (" << lista_obiektow[i]->polozenie() << ")" << std::endl;
      j++;
    }
    i++;
  }
}

int Scena::znajdzIdDrona(unsigned int id)
{
  unsigned int i = 0, j = 0;
  for (std::shared_ptr<Inter_krajobrazu> element : lista_obiektow)
  {
    if (std::dynamic_pointer_cast<Inter_drona>(element) != nullptr)
    {
      if (i == id)
      {
        return j;
      }
      i++;
    }
    j++;
  }
  return -1;
}

int Scena::znajdzIdPrzeszkody(unsigned int id)
{
  unsigned int i = 0, j = 0;
  for (std::shared_ptr<Inter_krajobrazu> element : lista_obiektow)
  {
    if (std::dynamic_pointer_cast<Przeszkoda>(element) != nullptr)
    {
      if (i == id)
      {
        return j;
      }
      i++;
    }
    j++;
  }
  return -1;
}

void Scena::usunDrona(unsigned int id)
{
  lista_obiektow.erase(lista_obiektow.begin() + this->znajdzIdDrona(id));
}

void Scena::usunPrzeszkode(unsigned int id)
{
  lista_obiektow.erase(lista_obiektow.begin() + this->znajdzIdPrzeszkody(id));
}

/*
void Scena::dodaj_wzgorze(Wektor<3> polozenie, double wysokosc, double min, double max){
    OstroslupN *O = new OstroslupN(polozenie, MacierzRot<3>(), wysokosc ,{min, max});
    //OstroslupN O(polozenie, MacierzRot<3>(), wysokosc, {min, max});
    lista_ostroslup.push_back(O);
    O->rysuj();

}
void Scena::dodaj_plaskowyz(Wektor<3> polozenie, double wysokosc, double min, double max){
    GraniastoslupN *G = new GraniastoslupN(polozenie, MacierzRot<3>(), wysokosc ,{min, max});
    //GraniastoslupN G(polozenie, MacierzRot<3>(), wysokosc, {min, max});
    lista_graniastoslup.push_back(G);
    G->rysuj();

}

void Scena::dodaj_drona(Wektor<3> polozenie){
    Dron *dron = new Dron(polozenie, MacierzRot<3>());
    lista_dron.push_back(dron);
    dron->rysuj();
}
*/