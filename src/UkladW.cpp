#include "UkladW.hh"

Wektor<3> UkladW::przelicz_punkt_do_rodzica(Wektor<3> punkt_lokalny){
    return (this->_orientacja * punkt_lokalny) + this->_srodek;
}

Wektor<3> UkladW::przelicz_do_globalnego(Wektor<3> punkt_lokalny) {
    if(this->poprzednik != nullptr){
        punkt_lokalny = przelicz_punkt_do_rodzica(punkt_lokalny);
        return (this->poprzednik->przelicz_punkt_do_rodzica(punkt_lokalny));
    }
    return przelicz_punkt_do_rodzica(punkt_lokalny);
}

drawNS::Point3D UkladW::konwersja(Wektor<3> wektor){
    return drawNS::Point3D(wektor[0], wektor[1], wektor[2]);
}