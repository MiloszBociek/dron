var searchData=
[
  ['apignuplot2d_0',['APIGnuPlot2D',['../classdraw_n_s_1_1_a_p_i_gnu_plot2_d.html#ad3be0fbba06cd36a882399a20e21393e',1,'drawNS::APIGnuPlot2D::APIGnuPlot2D()=delete'],['../classdraw_n_s_1_1_a_p_i_gnu_plot2_d.html#a5e8499cecdf3006ecabb888161e5caed',1,'drawNS::APIGnuPlot2D::APIGnuPlot2D(double minX, double maxX, double minY, double maxY, int ref_time_ms=0)'],['../classdraw_n_s_1_1_a_p_i_gnu_plot2_d.html',1,'drawNS::APIGnuPlot2D']]],
  ['apignuplot3d_1',['APIGnuPlot3D',['../classdraw_n_s_1_1_a_p_i_gnu_plot3_d.html#adeb697074546f7a00a0f29a9d34420f9',1,'drawNS::APIGnuPlot3D::APIGnuPlot3D()=delete'],['../classdraw_n_s_1_1_a_p_i_gnu_plot3_d.html#a9fd786880bdcfc90d260127a83362a01',1,'drawNS::APIGnuPlot3D::APIGnuPlot3D(double minX, double maxX, double minY, double maxY, double minZ, double maxZ, int ref_time_ms=0)'],['../classdraw_n_s_1_1_a_p_i_gnu_plot3_d.html',1,'drawNS::APIGnuPlot3D']]]
];
