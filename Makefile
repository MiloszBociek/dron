#
#  To sa opcje dla kompilacji
#
#CXXFLAGS=-g -Iinc -Isrc -Wall -pedantic -std=c++0x
CXXFLAGS=-g -Iinc -Isrc -Wall -pedantic -std=c++17
#__start__: obroty_3D
#	./obroty_3D

dron_3D: obj obj/main.o obj/MacierzRot.o obj/Wektor.o obj/Dr3D_gnuplot_api.o  obj/Dron.o  obj/Prostopadloscian.o obj/Blok.o\
 	obj/Graniastoslup6.o obj/UkladW.o obj/Powierzchnia.o obj/Inter_rysowania.o obj/Przeszkoda.o obj/Plaskowyz.o obj/Wzgorze.o obj/GraniastoslupPrawidlowy.o obj/Scena.o \

	g++ -Wall -pedantic -std=c++0x -o dron_3D obj/main.o obj/Wektor.o\
        obj/MacierzRot.o obj/Dr3D_gnuplot_api.o obj/Dron.o obj/Prostopadloscian.o obj/Blok.o obj/Graniastoslup6.o \
		obj/UkladW.o obj/Powierzchnia.o obj/Inter_rysowania.o obj/Przeszkoda.o obj/Plaskowyz.o obj/Wzgorze.o obj/GraniastoslupPrawidlowy.o obj/Scena.o -lpthread
				

obj:
	mkdir obj

obj/main.o: src/main.cpp inc/MacierzRot.hh inc/Wektor.hh inc/Blok.hh inc/Dr3D_gnuplot_api.hh inc/Inter_rysowania.hh
	g++ -c ${CXXFLAGS} -o obj/main.o src/main.cpp


obj/MacierzRot.o: src/MacierzRot.cpp inc/MacierzRot.hh 
	g++ -c ${CXXFLAGS} -o obj/MacierzRot.o src/MacierzRot.cpp

obj/Wektor.o: src/Wektor.cpp inc/Wektor.hh
	g++ -c ${CXXFLAGS} -o obj/Wektor.o src/Wektor.cpp

obj/Dron.o: src/Dron.cpp inc/Dron.hh
	g++ -c ${CXXFLAGS} -o obj/Dron.o src/Dron.cpp

obj/Prostopadloscian.o: src/Prostopadloscian.cpp inc/Prostopadloscian.hh
	g++ -c ${CXXFLAGS} -o obj/Prostopadloscian.o src/Prostopadloscian.cpp

obj/Blok.o: src/Blok.cpp inc/Blok.hh
	g++ -c ${CXXFLAGS} -o obj/Blok.o src/Blok.cpp

obj/Graniastoslup6.o: src/Graniastoslup6.cpp inc/Graniastoslup6.hh
	g++ -c ${CXXFLAGS} -o obj/Graniastoslup6.o src/Graniastoslup6.cpp

obj/UkladW.o: src/UkladW.cpp inc/UkladW.hh
	g++ -c ${CXXFLAGS} -o obj/UkladW.o src/UkladW.cpp

obj/Inter_rysowania.o: src/Inter_rysowania.cpp inc/Inter_rysowania.hh
	g++ -c ${CXXFLAGS} -o obj/Inter_rysowania.o src/Inter_rysowania.cpp

obj/Przeszkoda.o: src/Przeszkoda.cpp inc/Przeszkoda.hh
	g++ -c ${CXXFLAGS} -o obj/Przeszkoda.o src/Przeszkoda.cpp

#obj/Inter_krajobrazu.o: src/Inter_krajobrazu.cpp inc/Inter_krajobrazu.hh
#	g++ -c ${CXXFLAGS} -o obj/Inter_krajobrazu.o src/Inter_krajobrazu.cpp

#obj/Inter_drona.o: src/Inter_drona.cpp inc/Inter_drona.hh
#	g++ -c ${CXXFLAGS} -o obj/Inter_drona.o src/Inter_drona.cpp

obj/Wzgorze.o: src/Wzgorze.cpp inc/Wzgorze.hh
	g++ -c ${CXXFLAGS} -o obj/Wzgorze.o src/Wzgorze.cpp

obj/Plaskowyz.o: src/Plaskowyz.cpp inc/Plaskowyz.hh
	g++ -c ${CXXFLAGS} -o obj/Plaskowyz.o src/Plaskowyz.cpp

obj/GraniastoslupPrawidlowy.o: src/GraniastoslupPrawidlowy.cpp inc/GraniastoslupPrawidlowy.hh
	g++ -c ${CXXFLAGS} -o obj/GraniastoslupPrawidlowy.o src/GraniastoslupPrawidlowy.cpp

obj/Powierzchnia.o: src/Powierzchnia.cpp inc/Powierzchnia.hh
	g++ -c ${CXXFLAGS} -o obj/Powierzchnia.o src/Powierzchnia.cpp

obj/Scena.o: src/Scena.cpp inc/Scena.hh
	g++ -c ${CXXFLAGS} -o obj/Scena.o src/Scena.cpp

obj/Dr3D_gnuplot_api.o: src/Dr3D_gnuplot_api.cpp inc/Dr3D_gnuplot_api.hh
	g++ -c ${CXXFLAGS} -o obj/Dr3D_gnuplot_api.o src/Dr3D_gnuplot_api.cpp

clean:
	rm -f obj/*.o dron_3D